import { TokenSwap } from '@rockbot/shared/types';

export const generateMessageTextFromSwapData = (swap: TokenSwap): string => {
  const message = `${swap?.tokenAmountOut} ${swap?.tokenOutSym} for ${swap?.tokenAmountIn} ${swap?.tokenInSym}`;
  return message;
};
