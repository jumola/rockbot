export const environment = {
  // API_BASE_URL: 'http://localhost:3333/api',
  API_BASE_URL: 'http://rockbot-api.jumola.com/api',
  APP_NAME: 'RockBot',

  // production: false,
  // JWT_SECRET: '12345',
  // COOKIE_SECRET: '12345',
  // MONGO_URI:
  //   'mongodb://admin:admin@localhost:27017/apps?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false',

  // JWT_SERVER_NAME: 'test',
  // TOKEN_IDLE_LIFE_SECS: '999999999',
  // SERVER_URL: 'http://localhost:3333',
  // WEB_URL: 'http://localhost:4200',
  // // FOR TYPEORM / MYSQL
  // MYSQL_USER: 'root',
  // MYSQL_PASS: '',
  // MYSQL_DB: 'mbx',
  // MYSQL_PORT: 3306,
};
