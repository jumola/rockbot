import {
  forkJoin,
  from,
  Observable,
  ObservableInput,
  ObservedValueOf,
} from 'rxjs';
import { shareReplay, take, tap, timeout } from 'rxjs/operators';
import { ObjectType } from 'typeorm';

/**
 * Use to bypass circular reference issue for TypeORM by hiding the reference
 * and referring to ForwardRef instead.
 *
 * We use the typescript decorator setting, which adds some extra data to the
 * compiled code to let us lookup at runtime what type we set at design time.
 *
 * If you get a circular reference issue, it probably looks like this:
 *
 * ReferenceError: Cannot access 'Client' before initialization
 *
 * When you use "ForwardRef" to hide the type from the compiled code, it compiles
 * into safe code because the design type attempts to use "ForwardRef" (which is
 * only a type and safely resolves to undefined) instead of another entity Class
 * (which does exist, and if attempted to retrieve too early, causes the ReferenceError).
 * The compiled code using ForwardRef looks something like this:
 *
 * ```typescript
 * Object(tslib__WEBPACK_IMPORTED_MODULE_0__['__metadata'])(
 *   'design:type',
 *   typeof (_a =
 *     typeof _util_forward_ref__WEBPACK_IMPORTED_MODULE_6__['ForwardRef'] !==
 *       'undefined' &&
 *     _util_forward_ref__WEBPACK_IMPORTED_MODULE_6__['ForwardRef']) === 'function'
 *     ? _a
 *     : Object
 * );
 * ```
 */
export type ForwardRef<T> = T;

// suppress the error message:
// WARNING in ./libs/typeorm-server/src/lib/entity/AddressInformation.ts 18:50-60
// "export 'ForwardRef' was not found in '@mylbx/typeorm-shared'
export const ForwardRef = undefined;

/**
 * To overcome the other forward ref issue, that even referring in
 * "type => Client" kind of function causes an error, we add support
 * to specify in promises and load the ormconfig.js file by promise too.
 */
const forwardRefSet = new Set<Observable<any>>();
export const waitForForwardRefs = async () => {
  await forkJoin(Array.from(forwardRefSet)).toPromise();
};
export const forwardRef = <T extends ObservableInput<ObjectType<any>>>(
  typeInput: T
): ((type?: any) => ObservedValueOf<T>) => {
  // console.log('forwardRef', typeInput);

  let resolved: ObjectType<T> | undefined;

  const type$ = from(typeInput).pipe(
    take(1),
    timeout(1000),
    tap((value) => {
      // console.log('Got value', typeInput, value);
      resolved = value;
    }),
    shareReplay({
      bufferSize: 1,

      // if we set the true, it can be resubscribed any time,
      // we only want the source to be subscribed once, the first
      // time as soon as possible. We just use shareReply
      // to share the status (and any errors) to followup later,
      // so we set to false.
      refCount: false,
    })
  );

  forwardRefSet.add(type$);

  // tslint:disable-next-line: no-non-null-assertion
  const fn: any = (type) => resolved!;
  return fn;
};
