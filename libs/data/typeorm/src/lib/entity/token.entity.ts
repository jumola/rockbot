import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ForwardRef } from '@rockbot/data/typeorm-shared';
import { Channel } from './channel.entity';

@Entity()
export class Token {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name!: string;

  @Column()
  ticker!: string;

  @Column()
  address!: string;

  @ManyToOne(() => Channel)
  channel!: ForwardRef<Channel>;
  @Column({ nullable: false })
  channelId!: number;

  @Column({ default: false })
  isActive!: boolean;

  // meta data
  @CreateDateColumn()
  createdAt!: Date;

  @UpdateDateColumn()
  updatedAt!: Date;
}
