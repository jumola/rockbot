import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Channel } from './channel.entity';
import { ForwardRef } from '@rockbot/data/typeorm-shared';
import { Token } from './token.entity';
import { MessageType } from '@rockbot/shared/types';

@Entity()
export class Message {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Channel)
  channel!: ForwardRef<Channel>;
  @Column({ nullable: false })
  channelId!: number;

  @ManyToOne(() => Token)
  token!: ForwardRef<Token>;
  @Column({ nullable: false })
  tokenId!: number;

  @Column({ nullable: true })
  blockchainTransactionId!: string;

  @Column({ default: MessageType.Announcement, type: 'simple-enum', enum: MessageType })
  type!: MessageType;

  @Column()
  text!: string;

  // meta data
  @CreateDateColumn()
  createdAt!: Date;

  @UpdateDateColumn()
  updatedAt!: Date;
}
