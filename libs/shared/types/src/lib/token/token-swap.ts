export interface TokenSwap {
  id: string;
  timestamp: string;
  tokenInSym: string;
  tokenOutSym: string;
  tokenAmountIn: string;
  tokenAmountOut: string;
}
