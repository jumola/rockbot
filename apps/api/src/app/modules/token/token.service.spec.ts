import { Test, TestingModule } from '@nestjs/testing';
import { Token } from '@rockbot/data/typeorm';
import { AppModule } from '../../app.module';
import { TokenModule } from './token.module';
import { TokenService } from './token.service';

describe('TokenService', () => {
  let service: TokenService;
  let doc: Token;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule, TokenModule],
      providers: [TokenService],
    }).compile();

    service = module.get<TokenService>(TokenService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create token', async () => {
    const response: Token = await service.create({
      name: 'TOKEN NAME',
      ticker: 'TEST',
      address: '1234',
    });
    expect(response).toBeDefined();
    expect(response.id).toBeDefined();
    doc = response;
    expect(doc).toBeDefined();
  });

  it('should find token', async () => {
    const response: Token = await service.findOne(doc?.id);
    expect(response).toBeDefined();
    expect(response.id).toEqual(doc.id);
  });

  it('should find channels', async () => {
    const response: Token[] = await service.findAll({});
    expect(response).toBeDefined();
    expect(response?.length).toBeGreaterThanOrEqual(0);
  });

  it('should update token', async () => {
    const newParams: Partial<Token> = {
      name: 'new name',
      ticker: 'TEST12',
      address: '2345678',
    };
    const response: Token = await service.update(doc?.id, newParams);
    expect(response).toBeDefined();
    expect(response.name).toEqual(newParams.name);
    expect(response.ticker).toEqual(newParams.ticker);
    expect(response.address).toEqual(newParams.address);
  });

  it('should delete token', async () => {
    await service.remove(doc?.id);
    const response = await service.findOne(doc?.id);
    expect(response).toBeFalsy();
  });

  it('should test getting latest swap', async () => {
    const response = await service.getLatestSwapInVswap(
      '0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c'
    );
    expect(response).toBeDefined();
  });
});
