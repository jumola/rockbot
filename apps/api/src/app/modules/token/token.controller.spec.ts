import { Test, TestingModule } from '@nestjs/testing';
import { Token } from '@rockbot/data/typeorm';
import { AppModule } from '../../app.module';
import { TokenController } from './token.controller';
import { TokenService } from './token.service';
import { TokenModule } from '../token/token.module';

describe('TokenController', () => {
  let controller: TokenController;
  let doc: Token;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule, TokenModule],
      controllers: [TokenController],
      providers: [TokenService],
    }).compile();

    controller = module.get<TokenController>(TokenController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create token', async () => {
    const response: Token = await controller.create({
      name: 'new Name test',
      ticker: 'TEST',
      address: '123',
    });
    expect(response).toBeDefined();
    expect(response.id).toBeDefined();
    doc = response;
    expect(doc).toBeDefined();
  });

  it('should find channel', async () => {
    const response: Token = await controller.findOne(String(doc?.id));
    expect(response).toBeDefined();
    expect(response.id).toEqual(doc.id);
  });

  it('should find channels', async () => {
    const response: Token[] = await controller.findAll();
    expect(response).toBeDefined();
    expect(response?.length).toBeGreaterThanOrEqual(0);
  });

  it('should update channel', async () => {
    const newParams = {
      name: 'updatedName ',
      ticker: 'TEST2',
      address: '4567',
    } as Partial<Token>;
    const response: Token = await controller.update(String(doc?.id), newParams);
    expect(response).toBeDefined();
    expect(response.name).toEqual(newParams.name);
    expect(response.ticker).toEqual(newParams.ticker);
    expect(response.address).toEqual(newParams.address);
  });

  it('should delete channel', async () => {
    await controller.remove(String(doc?.id));
    const response = await controller.findOne(String(doc?.id));
    expect(response).toBeFalsy();
  });
});
