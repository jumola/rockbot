import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Token } from '@rockbot/data/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { request, gql } from 'graphql-request';

@Injectable()
export class TokenService {
  defaultRelations = ['channel'];

  constructor(
    @InjectRepository(Token)
    private repo: Repository<Token>
  ) {}

  async create(data: Partial<Token>): Promise<Token> {
    return await this.repo.save(data);
  }

  async findAll(params?): Promise<Token[]> {
    return await this.repo.find({
      where: params ?? {},
      relations: this.defaultRelations,
    });
  }

  async findOne(id): Promise<Token> {
    return await this.repo.findOne(id, { relations: this.defaultRelations });
  }

  async findOneWhere(params: Partial<Token>): Promise<Token> {
    return await this.repo.findOne({
      where: params,
    });
  }

  async update(id: number, data: Partial<Token>): Promise<Token> {
    const item = await this.findOne(id);
    return await this.repo.save({
      ...item,
      ...data,
    });
  }

  async remove(id: number) {
    return await this.repo.delete(id);
  }

  // custom services

  /** gets the latest swap in the specified token contract address via vswap  */
  async getLatestSwapInVswap(address: string) {
    const query = gql`
      {
        swaps(
          first: 1
          orderBy: timestamp
          orderDirection: desc
          where: {
            tokensList_contains: ["${address}"]
          }
        ) {
          id
          timestamp
          tokenInSym
          tokenOutSym
          tokenAmountIn
          tokenAmountOut
          value
          feeValue
        }
      }
    `;

    const response = await request(
      'https://api.bscgraph.org/subgraphs/name/vswap/exchange-pair',
      query
    );
    return response;
  }
}
