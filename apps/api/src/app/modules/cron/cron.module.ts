import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Channel, Message, Token } from '@rockbot/data/typeorm';
import { ChannelModule } from '../channel/channel.module';
import { MessageModule } from '../message/message.module';
import { TokenModule } from '../token/token.module';
import { CronService } from './cron.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Channel, Token, Message]),
    TokenModule,
    ChannelModule,
    MessageModule,
  ],
  providers: [CronService],
  exports: [TypeOrmModule, CronService],
})
export class CronModule {}
