import { Injectable, Logger } from '@nestjs/common';
import { Token, Message } from '@rockbot/data/typeorm';
import { ChannelService } from '../channel/channel.service';
import { TokenService } from '../token/token.service';
import { Cron } from '@nestjs/schedule';
import { MessageType, TokenSwap } from '@rockbot/shared/types';
import { generateMessageTextFromSwapData } from '@rockbot/features/bsc/api';
import { MessageService } from '../message/message.service';
import { compact, isNil } from 'lodash';

@Injectable()
export class CronService {
  private readonly logger = new Logger(CronService.name);

  constructor(
    private channelService: ChannelService,
    private tokenService: TokenService,
    private messageService: MessageService
  ) {}

  async bscNetwork() {
    // const web3 = new Web3(
    //   new Web3.providers.HttpProvider('https://bsc-dataseed1.binance.org:443')
    // );
    // const account = web3.eth.accounts.create();
    // console.log('account', account);

    return 'asdad';
  }

  /** send a message to a channel */
  // @Cron('* * * * * *')
  async broadcastSwap() {
    // first, get the active tokens
    const tokensRaw = await this.tokenService.findAll({
      isActive: true,
    });

    const tokens: Token[] = (tokensRaw ?? []).filter((token) => token?.address);

    for (const token of tokens) {
      const response = await this.tokenService.getLatestSwapInVswap(
        token.address
      );

      const swap: TokenSwap | undefined = response?.swaps?.length
        ? (response.swaps[0] as TokenSwap)
        : undefined;

      if (swap) {
        // make sure this transaction hasn't been processed yet
        const params: Partial<Message> = {
          blockchainTransactionId: swap.id,
        };

        const messageByTxId = await this.messageService.findOneWhere(params);

        // skip already broadcasted
        if (isNil(messageByTxId)) {
          // create a new message document
          const newMessage: Partial<Message> = {
            channelId: token.channelId,
            tokenId: token.id,
            blockchainTransactionId: swap.id,
            type: MessageType.Swap,
            text: generateMessageTextFromSwapData(swap),
          };
          await this.messageService.create(newMessage);

          await this.channelService.sendTelegramMessage(
            token?.channel?.telegramChannelId,
            newMessage.text
          );
        }
      }
    }
    return;
  }
}
