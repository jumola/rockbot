import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../../app.module';
import { ChannelModule } from '../channel/channel.module';
import { MessageModule } from '../message/message.module';
import { TokenModule } from '../token/token.module';
import { CronService } from './cron.service';

describe('CronService', () => {
  let service: CronService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule, TokenModule, ChannelModule, MessageModule],
      providers: [CronService],
    }).compile();

    service = module.get<CronService>(CronService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should test bsc network', async () => {
    const response = await service.bscNetwork();
    console.log('response', response);
  });

  it.skip('should broadcast swaps', async () => {
    const result = await service.broadcastSwap();
    expect(result).toBeDefined();
  });
});
