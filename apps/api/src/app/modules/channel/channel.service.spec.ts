import { Test, TestingModule } from '@nestjs/testing';
import { Channel } from '@rockbot/data/typeorm';
import { ChannelModule } from './channel.module';
import { ChannelService } from './channel.service';
import { AppModule } from '../../app.module';

describe('ChannelService', () => {
  let service: ChannelService;
  let doc: Channel;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule, ChannelModule],
      providers: [ChannelService],
    }).compile();
    service = module.get<ChannelService>(ChannelService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create channel', async () => {
    const response: Channel = await service.create({
      telegramChannelId: 'testChannelId',
    });
    expect(response).toBeDefined();
    expect(response.id).toBeDefined();
    doc = response;
    expect(doc).toBeDefined();
  });

  it('should find channel', async () => {
    const response: Channel = await service.findOne(doc?.id);
    expect(response).toBeDefined();
    expect(response.id).toEqual(doc.id);
  });

  it('should find channels', async () => {
    const response: Channel[] = await service.findAll({});
    expect(response).toBeDefined();
    expect(response?.length).toBeGreaterThanOrEqual(0);
  });

  it('should update channel', async () => {
    const newParams = {
      telegramChannelId: 'newChannelIdTest',
    } as Partial<Channel>;
    const response: Channel = await service.update(doc?.id, newParams);
    expect(response).toBeDefined();
    expect(response.telegramChannelId).toEqual(newParams.telegramChannelId);
  });

  it('should send to telegram', async () => {
    const chat_id = '@kittymal2';
    const text = 'testing message';
    const response = (await service.sendTelegramMessage(chat_id, text)) as any;
    expect(response?.data?.ok).toEqual(true);
  });

  it('should delete channel', async () => {
    await service.remove(doc?.id);
    const response = await service.findOne(doc?.id);
    expect(response).toBeFalsy();
  });
});
