import { Injectable } from '@nestjs/common';
import { CreateChannelDto } from './dto/create-channel.dto';
import { UpdateChannelDto } from './dto/update-channel.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Channel } from '@rockbot/data/typeorm';
import { Repository } from 'typeorm';
import { TELEGRAM_API_KEY } from '@rockbot/data/constants';
import axios from 'axios';

@Injectable()
export class ChannelService {
  constructor(
    @InjectRepository(Channel)
    private repo: Repository<Channel>
  ) {}

  async create(data: CreateChannelDto): Promise<Channel> {
    return await this.repo.save(data);
  }

  async findAll(params?): Promise<Channel[]> {
    return await this.repo.find({
      where: params || {},
    });
  }

  async findOne(id: number): Promise<Channel> {
    return await this.repo.findOne(id);
  }

  async update(id: number, data: Partial<Channel>): Promise<Channel> {
    const item = await this.findOne(id);
    return await this.repo.save({
      ...item,
      ...data,
    });
  }

  async remove(id: number) {
    await this.repo.delete(id);
  }

  async sendTelegramMessage(chat_id, text) {
    const url = `https://api.telegram.org/${TELEGRAM_API_KEY}/sendMessage`;
    const params = { chat_id, text };
    return await axios.get(url, { params });
  }
}
