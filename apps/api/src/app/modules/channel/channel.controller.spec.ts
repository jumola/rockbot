import { Test, TestingModule } from '@nestjs/testing';
import { Channel } from '@rockbot/data/typeorm';
import { AppModule } from '../../app.module';
import { ChannelController } from './channel.controller';
import { ChannelModule } from './channel.module';
import { ChannelService } from './channel.service';
import { UpdateChannelDto } from './dto/update-channel.dto';

describe('ChannelController', () => {
  let controller: ChannelController;
  let doc: Channel;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule, ChannelModule],
      controllers: [ChannelController],
      providers: [ChannelService],
    }).compile();

    controller = module.get<ChannelController>(ChannelController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create channel', async () => {
    const response: Channel = await controller.create({
      telegramChannelId: 'testChannelId',
    });
    expect(response).toBeDefined();
    expect(response.id).toBeDefined();
    doc = response;
    expect(doc).toBeDefined();
  });

  it('should find channel', async () => {
    const response: Channel = await controller.findOne(String(doc?.id));
    expect(response).toBeDefined();
    expect(response.id).toEqual(doc.id);
  });

  it('should find channels', async () => {
    const response: Channel[] = await controller.findAll();
    expect(response).toBeDefined();
    expect(response?.length).toBeGreaterThanOrEqual(0);
  });

  it('should update channel', async () => {
    const newParams = {
      telegramChannelId: 'newChannelIdTest',
    } as UpdateChannelDto;
    const response: Channel = await controller.update(
      String(doc?.id),
      newParams
    );
    expect(response).toBeDefined();
    expect(response.telegramChannelId).toEqual(newParams.telegramChannelId);
  });

  it('should delete channel', async () => {
    await controller.remove(String(doc?.id));
    const response = await controller.findOne(String(doc?.id));
    expect(response).toBeFalsy();
  });
});
