import { Module } from '@nestjs/common';
import { MessageService } from './message.service';
import { MessageController } from './message.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Channel, Message, Token } from '@rockbot/data/typeorm';
import { TokenModule } from '../token/token.module';
import { ChannelModule } from '../channel/channel.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Message, Channel, Token]),
    TokenModule,
    ChannelModule,
  ],
  controllers: [MessageController],
  providers: [MessageService],
  exports: [TypeOrmModule, MessageService],
})
export class MessageModule {}
