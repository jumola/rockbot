import { Test, TestingModule } from '@nestjs/testing';
import { Channel, Message, Token } from '@rockbot/data/typeorm';
import { MessageType } from '@rockbot/shared/types';
import { AppModule } from '../../app.module';
import { ChannelModule } from '../channel/channel.module';
import { ChannelService } from '../channel/channel.service';
import { TokenModule } from '../token/token.module';
import { TokenService } from '../token/token.service';
import { MessageModule } from './message.module';
import { MessageService } from './message.service';

describe('MessageService', () => {
  let service: MessageService;
  let channelService: ChannelService;
  let tokenService: TokenService;
  let doc: Message;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule, MessageModule, ChannelModule, TokenModule],
      providers: [MessageService],
    }).compile();

    service = module.get<MessageService>(MessageService);
    channelService = module.get<ChannelService>(ChannelService);
    tokenService = module.get<TokenService>(TokenService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create message', async () => {
    const channels: Channel[] = await channelService.findAll({});
    const channel: Channel = channels[0];

    const tokens: Token[] = await tokenService.findAll({});
    const token: Token = tokens[0];

    const response: Message = await service.create({
      channelId: channel?.id,
      tokenId: token?.id,
      blockchainTransactionId: '12345Test',
      text: 'test',
      type: MessageType.Announcement,
    } as Message);

    expect(response).toBeDefined();
    expect(response.id).toBeDefined();
    doc = response;
    expect(doc).toBeDefined();
  });

  it('should find message', async () => {
    const response: Message = await service.findOne(doc?.id);
    expect(response).toBeDefined();
    expect(response.id).toEqual(doc.id);
  });

  it('should find messages', async () => {
    const response: Message[] = await service.findAll({});
    expect(response).toBeDefined();
    expect(response?.length).toBeGreaterThanOrEqual(0);
  });

  it('should update message', async () => {
    const newParams = {
      blockchainTransactionId: 'new_blockchainTransactionId',
    } as Message;
    const response: Message = await service.update(doc?.id, newParams);
    expect(response).toBeDefined();
    expect(response.blockchainTransactionId).toEqual(
      newParams.blockchainTransactionId
    );
  });

  it('should delete message', async () => {
    await service.remove(doc?.id);
    const response = await service.findOne(doc?.id);
    expect(response).toBeFalsy();
  });
});
