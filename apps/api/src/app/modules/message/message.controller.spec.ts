import { Test, TestingModule } from '@nestjs/testing';
import { Channel, Message, Token } from '@rockbot/data/typeorm';
import { MessageType } from '../../../../../../libs/shared/types/src';
import { AppModule } from '../../app.module';
import { ChannelModule } from '../channel/channel.module';
import { ChannelService } from '../channel/channel.service';
import { TokenModule } from '../token/token.module';
import { TokenService } from '../token/token.service';
import { MessageController } from './message.controller';
import { MessageModule } from './message.module';
import { MessageService } from './message.service';

describe('MessageController', () => {
  let controller: MessageController;
  let channelService: ChannelService;
  let tokenService: TokenService;
  let doc: Message;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule, MessageModule, ChannelModule, TokenModule],
      controllers: [MessageController],
      providers: [MessageService],
    }).compile();

    controller = module.get<MessageController>(MessageController);
    channelService = module.get<ChannelService>(ChannelService);
    tokenService = module.get<TokenService>(TokenService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create message', async () => {
    const channels: Channel[] = await channelService.findAll({});
    const message: Channel = channels[0];

    const tokens: Token[] = await tokenService.findAll({});
    const token: Token = tokens[0];

    const response: Message = await controller.create({
      channelId: message?.id,
      tokenId: token?.id,
      blockchainTransactionId: '12345Test',
      text: 'test',
      type: MessageType.Announcement,
    } as Message);
    expect(response).toBeDefined();
    expect(response.id).toBeDefined();
    doc = response;
    expect(doc).toBeDefined();
  });

  it('should find message', async () => {
    const response: Message = await controller.findOne(String(doc?.id));
    expect(response).toBeDefined();
    expect(response.id).toEqual(doc.id);
  });

  it('should find channels', async () => {
    const response: Message[] = await controller.findAll();
    expect(response).toBeDefined();
    expect(response?.length).toBeGreaterThanOrEqual(0);
  });

  it('should update message', async () => {
    const newParams = {
      blockchainTransactionId: 'new_blockchainTransactionId',
    } as Message;
    const response: Message = await controller.update(
      String(doc?.id),
      newParams
    );
    expect(response).toBeDefined();
    expect(response.blockchainTransactionId).toEqual(
      newParams.blockchainTransactionId
    );
  });

  it('should delete message', async () => {
    await controller.remove(String(doc?.id));
    const response = await controller.findOne(String(doc?.id));
    expect(response).toBeFalsy();
  });
});
