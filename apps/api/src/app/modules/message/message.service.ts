import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Message } from '@rockbot/data/typeorm';

@Injectable()
export class MessageService {
  constructor(
    @InjectRepository(Message)
    private repo: Repository<Message>
  ) {}

  async create(data: Partial<Message>): Promise<Message> {
    return await this.repo.save(data);
  }

  async findAll(params?): Promise<Message[]> {
    return await this.repo.find({
      where: params || {},
    });
  }

  async findOne(id): Promise<Message> {
    return await this.repo.findOne(id);
  }

  async findOneWhere(params: Partial<Message>): Promise<Message> {
    return await this.repo.findOne({
      where: params,
    });
  }

  async update(id: number, data: Partial<Message>): Promise<Message> {
    const item = await this.findOne(id);
    return await this.repo.save({
      ...item,
      ...data,
    });
  }

  async remove(id: number) {
    await this.repo.delete(id);
  }
}
