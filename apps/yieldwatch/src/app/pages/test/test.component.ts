import { Component, Inject, InjectionToken, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss'],
})
export class TestComponent implements OnInit {
  ngOnInit() {
    console.log('ngOnInit');
  }
}
