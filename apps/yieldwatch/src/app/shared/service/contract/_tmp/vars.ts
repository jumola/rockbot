import { AbiItem } from 'web3-utils';

export const PANCAKE_CHEF_ADDR = '0x73feaa1eE314F8c655E354234017bE2193C9E24E';

export const PANCAKE_CHEF_ABI: AbiItem[] = [
  {
    inputs: [
      { internalType: 'contract CakeToken', name: '_cake', type: 'address' },
      { internalType: 'contract SyrupBar', name: '_syrup', type: 'address' },
      { internalType: 'address', name: '_devaddr', type: 'address' },

      {
        internalType: 'uint256',
        name: '_cakePerBlock',
        type: 'uint256',
      },

      { internalType: 'uint256', name: '_startBlock', type: 'uint256' },
    ],
    stateMutability: 'nonpayable',
    type: 'constructor',
  },
  {
    anonymous: false,
    inputs: [
      { indexed: true, internalType: 'address', name: 'user', type: 'address' },
      { indexed: true, internalType: 'uint256', name: 'pid', type: 'uint256' },
      {
        indexed: false,
        internalType: 'uint256',
        name: 'amount',
        type: 'uint256',
      },
    ],
    name: 'Deposit',
    type: 'event',
  },
  {
    anonymous: false,
    inputs: [
      { indexed: true, internalType: 'address', name: 'user', type: 'address' },
      { indexed: true, internalType: 'uint256', name: 'pid', type: 'uint256' },
      {
        indexed: false,
        internalType: 'uint256',
        name: 'amount',
        type: 'uint256',
      },
    ],
    name: 'EmergencyWithdraw',
    type: 'event',
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: 'address',
        name: 'previousOwner',
        type: 'address',
      },
      {
        indexed: true,
        internalType: 'address',
        name: 'newOwner',
        type: 'address',
      },
    ],
    name: 'OwnershipTransferred',
    type: 'event',
  },
  {
    anonymous: false,
    inputs: [
      { indexed: true, internalType: 'address', name: 'user', type: 'address' },
      { indexed: true, internalType: 'uint256', name: 'pid', type: 'uint256' },
      {
        indexed: false,
        internalType: 'uint256',
        name: 'amount',
        type: 'uint256',
      },
    ],
    name: 'Withdraw',
    type: 'event',
  },
  {
    inputs: [],
    name: 'BONUS_MULTIPLIER',
    outputs: [{ internalType: 'uint256', name: '', type: 'uint256' }],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [
      { internalType: 'uint256', name: '_allocPoint', type: 'uint256' },
      { internalType: 'contract IBEP20', name: '_lpToken', type: 'address' },
      { internalType: 'bool', name: '_withUpdate', type: 'bool' },
    ],
    name: 'add',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [],
    name: 'cake',
    outputs: [
      { internalType: 'contract CakeToken', name: '', type: 'address' },
    ],
    stateMutability: 'view',
    type: 'function',
  },

  {
    inputs: [],
    name: 'cakePerBlock',
    outputs: [
      {
        internalType: 'uint256',
        name: '',
        type: 'uint256',
      },
    ],
    stateMutability: 'view',
    type: 'function',
  },

  {
    inputs: [
      { internalType: 'uint256', name: '_pid', type: 'uint256' },
      { internalType: 'uint256', name: '_amount', type: 'uint256' },
    ],
    name: 'deposit',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [{ internalType: 'address', name: '_devaddr', type: 'address' }],
    name: 'dev',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [],
    name: 'devaddr',
    outputs: [{ internalType: 'address', name: '', type: 'address' }],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [{ internalType: 'uint256', name: '_pid', type: 'uint256' }],
    name: 'emergencyWithdraw',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [{ internalType: 'uint256', name: '_amount', type: 'uint256' }],
    name: 'enterStaking',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      { internalType: 'uint256', name: '_from', type: 'uint256' },
      { internalType: 'uint256', name: '_to', type: 'uint256' },
    ],
    name: 'getMultiplier',
    outputs: [{ internalType: 'uint256', name: '', type: 'uint256' }],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [{ internalType: 'uint256', name: '_amount', type: 'uint256' }],
    name: 'leaveStaking',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [],
    name: 'massUpdatePools',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [{ internalType: 'uint256', name: '_pid', type: 'uint256' }],
    name: 'migrate',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [],
    name: 'migrator',
    outputs: [
      { internalType: 'contract IMigratorChef', name: '', type: 'address' },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [],
    name: 'owner',
    outputs: [{ internalType: 'address', name: '', type: 'address' }],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [
      { internalType: 'uint256', name: '_pid', type: 'uint256' },
      { internalType: 'address', name: '_user', type: 'address' },
    ],
    name: 'pendingCake',
    outputs: [{ internalType: 'uint256', name: '', type: 'uint256' }],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [{ internalType: 'uint256', name: '', type: 'uint256' }],
    name: 'poolInfo',
    outputs: [
      { internalType: 'contract IBEP20', name: 'lpToken', type: 'address' },
      { internalType: 'uint256', name: 'allocPoint', type: 'uint256' },
      { internalType: 'uint256', name: 'lastRewardBlock', type: 'uint256' },
      { internalType: 'uint256', name: 'accCakePerShare', type: 'uint256' },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [],
    name: 'poolLength',
    outputs: [{ internalType: 'uint256', name: '', type: 'uint256' }],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [],
    name: 'renounceOwnership',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      { internalType: 'uint256', name: '_pid', type: 'uint256' },
      { internalType: 'uint256', name: '_allocPoint', type: 'uint256' },
      { internalType: 'bool', name: '_withUpdate', type: 'bool' },
    ],
    name: 'set',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      {
        internalType: 'contract IMigratorChef',
        name: '_migrator',
        type: 'address',
      },
    ],
    name: 'setMigrator',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [],
    name: 'startBlock',
    outputs: [{ internalType: 'uint256', name: '', type: 'uint256' }],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [],
    name: 'syrup',
    outputs: [{ internalType: 'contract SyrupBar', name: '', type: 'address' }],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [],
    name: 'totalAllocPoint',
    outputs: [{ internalType: 'uint256', name: '', type: 'uint256' }],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [{ internalType: 'address', name: 'newOwner', type: 'address' }],
    name: 'transferOwnership',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      { internalType: 'uint256', name: 'multiplierNumber', type: 'uint256' },
    ],
    name: 'updateMultiplier',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [{ internalType: 'uint256', name: '_pid', type: 'uint256' }],
    name: 'updatePool',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    inputs: [
      { internalType: 'uint256', name: '', type: 'uint256' },
      { internalType: 'address', name: '', type: 'address' },
    ],
    name: 'userInfo',
    outputs: [
      { internalType: 'uint256', name: 'amount', type: 'uint256' },
      { internalType: 'uint256', name: 'rewardDebt', type: 'uint256' },
    ],
    stateMutability: 'view',
    type: 'function',
  },
  {
    inputs: [
      { internalType: 'uint256', name: '_pid', type: 'uint256' },
      { internalType: 'uint256', name: '_amount', type: 'uint256' },
    ],
    name: 'withdraw',
    outputs: [],
    stateMutability: 'nonpayable',
    type: 'function',
  },
];
export const bscTokens = [
  {
    id: 'wbnb',
    symbol: 'wbnb',
    contract: '0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c',
  },
  {
    id: 'binance-usd',
    symbol: 'busd',
    contract: '0xe9e7CEA3DedcA5984780Bafc599bD69ADd087D56',
  },
  {
    id: 'pancakeswap-token',
    symbol: 'CAKE',
    contract: '0x0e09fabb73bd3ade0a17ecc321fd13a19e81ce82',
  },
  {
    id: 'beefy-finance',
    symbol: 'BIFI',
    contract: '0xca3f508b8e4dd382ee878a314789373d80a5190a',
  },
  {
    id: 'bdollar-share',
    symbol: 'sBDO',
    contract: '0x0d9319565be7f53cefe84ad201be3f40feae2740',
  },
  {
    id: 'belugaswap',
    symbol: 'BELUGA',
    contract: '0x181de8c57c4f25eba9fd27757bbd11cc66a55d31',
  },
  {
    id: 'chainlink',
    symbol: 'LINK',
    contract: '0xf8a0bf9cf54bb92f17374d9e9a321e6a111a51bd',
  },
  {
    id: 'bscex',
    symbol: 'BSCX',
    contract: '0x5ac52ee5b2a633895292ff6d8a89bb9190451587',
  },
  {
    id: 'binance-eth',
    symbol: 'BETH',
    contract: '0x250632378e573c6be1ac2f97fcdf00515d0aa91b',
  },
  {
    id: 'tether',
    symbol: 'USDT',
    contract: '0x55d398326f99059fF775485246999027B3197955',
  },
  {
    id: 'bitcoin-bep2',
    symbol: 'BTCB',
    contract: '0x7130d2A12B9BCbFAe4f2634d864A1Ee1Ce3Ead9c',
  },
  {
    id: 'ethereum',
    symbol: 'ETH',
    contract: '0x2170Ed0880ac9A755fd29B2688956BD959F933F8',
  },
  {
    id: 'bakerytoken',
    symbol: 'BAKE',
    contract: '0xE02dF9e3e622DeBdD69fb838bB799E3F168902c5',
  },
  {
    id: 'goose-finance',
    symbol: 'EGG',
    contract: '0xf952fc3ca7325cc27d15885d37117676d25bfda6',
  },
  {
    id: 'dai',
    symbol: 'DAI',
    contract: '0x1af3f329e8be154074d8769d1ffa4ee058b1dbc3',
  },
  {
    id: 'auto',
    symbol: 'AUTO',
    contract: '0xa184088a740c695e156f91f5cc086a06bb78b827',
  },
  {
    id: 'wault-finance',
    symbol: 'WAULT',
    contract: '0x6ff2d9e5891a7a7c554b80e0d1b791483c78bce9',
  },
  {
    id: 'swipe',
    symbol: 'SXP',
    contract: '0x47BEAd2563dCBf3bF2c9407fEa4dC236fAbA485A',
  },
  {
    id: 'vai',
    symbol: 'VAI',
    contract: '0x4bd17003473389a42daf6a0a729f6fdb328bbbd7',
  },
  {
    id: 'venus',
    symbol: 'XVS',
    contract: '0xcF6BB5389c92Bdda8a3747Ddb454cB7a64626C63',
  },
  {
    id: 'terrausd',
    symbol: 'UST',
    contract: '0x23396cf899ca06c4472205fc903bdb4de249d6fc',
  },
  {
    id: 'cardano',
    symbol: 'ADA',
    contract: '0x3EE2200Efb3400fAbB9AacF31297cBdD1d435D47',
  },
  {
    id: 'bearn-fi',
    symbol: 'BFI',
    contract: '0x81859801b01764d4f0fa5e64729f5a6c3b91435b',
  },
  {
    id: 'polkadot',
    symbol: 'DOT',
    contract: '0x7083609fCE4d1d8Dc0C979AAb8c869Ea2C873402',
  },
  {
    id: 'vbswap',
    symbol: 'VBSWAP',
    contract: '0x4f0ed527e8a95ecaa132af214dfd41f30b361600',
  },
  {
    id: 'bdollar',
    symbol: 'BDO',
    contract: '0x190b589cf9fb8ddeabbfeae36a813ffb2a702454',
  },
  {
    id: 'julswap',
    symbol: 'JULD',
    contract: '0x5a41f637c3f7553dba6ddc2d3ca92641096577ea',
  },
  {
    id: 'the-famous-token',
    symbol: 'TFT',
    contract: '0xA9d3fa202b4915c3eca496b0e7dB41567cFA031C',
  },
  {
    id: 'shield-protocol',
    symbol: 'SHIELD',
    contract: '0x60b3bc37593853c04410c4f07fe4d6748245bf77',
  },
  {
    id: 'lead-token',
    symbol: 'LEAD',
    contract: '0x2ed9e96EDd11A1fF5163599A66fb6f1C77FA9C66',
  },
  {
    id: 'sparkpoint',
    symbol: 'SRK',
    contract: '0x3B1eC92288D78D421f97562f8D479e6fF7350a16',
  },
  {
    id: 'curate',
    symbol: 'XCUR',
    contract: '0x708C671Aa997da536869B50B6C67FA0C32Ce80B2',
  },
  {
    id: 'uniswap',
    symbol: 'UNI',
    contract: '0xBf5140A22578168FD562DCcF235E5D43A02ce9B1',
  },
  {
    id: 'tsuki-dao',
    symbol: 'TSUKI',
    contract: '0x3fd9e7041c45622e8026199a46f763c9807f66f3',
  },
  {
    id: 'panda-yield',
    symbol: 'BBOO',
    contract: '0xd909840613fcb0fadc6ee7e5ecf30cdef4281a68',
  },
  {
    id: 'cryptex',
    symbol: 'CRX',
    contract: '0x97a30C692eCe9C317235d48287d23d358170FC40',
  },
  {
    id: 'polis',
    symbol: 'POLIS',
    contract: '0xb5bea8a26d587cf665f2d78f077cca3c7f6341bd',
  },
  {
    id: 'tether',
    symbol: 'USDT',
    contract: '0x049d68029688eAbF473097a2fC38ef61633A3C7A',
  },
  {
    id: 'swirl-cash',
    symbol: 'SWIRL',
    contract: '0x52d86850bc8207b520340b7e39cdaf22561b9e56',
  },
  {
    id: 'squirrel-finance',
    symbol: 'NUTS',
    contract: '0x8893D5fA71389673C5c4b9b3cb4EE1ba71207556',
  },
  {
    id: 'usd-coin',
    symbol: 'USDC',
    contract: '0x8AC76a51cc950d9822D68b83fE1Ad97B32Cd580d',
  },
  {
    id: 'iron-stablecoin',
    symbol: 'IRON',
    contract: '0x7b65b489fe53fce1f6548db886c08ad73111ddd8',
  },
];
