import { Inject, Injectable } from '@angular/core';
import { chunk } from 'lodash';
import { BatchRequest, provider, Providers, Extension } from 'web3-core';
import Web3 from 'web3';
import { WEB3 } from '../../web3';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { PANCAKE_CHEF_ABI, PANCAKE_CHEF_ADDR, bscTokens } from './_tmp/vars';
import { AbiItem } from 'web3-utils';
import { Contract, ContractOptions } from 'web3-eth-contract';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ContractService {
  web3$ = new BehaviorSubject<Web3 | undefined>(undefined);

  userWalletAddress$ = new BehaviorSubject<string | undefined>(undefined);

  constructor(private http: HttpClient) {}

  get userWalletAddress() {
    return this.userWalletAddress.getValue();
  }

  get web3Provider() {
    return this.web3$.getValue().eth;
  }

  async getAccount(): Promise<string> {
    const accounts = await this.web3Provider.getAccounts();
    return accounts[0];
  }

  async getContract(abi: AbiItem[], address: string): Promise<Contract> {
    return await new this.web3Provider.Contract(abi, address);
  }

  async getRewardsPerWeek(contract: Contract, fnName: string) {
    return (((await contract.methods[fnName]().call()) / 1e18) * 604800) / 3;
  }

  lookUpPrices$(url: string): Observable<any> {
    return this.http.get<any>(url).pipe();
  }

  async lookUpPrices(id_array) {
    const prices = {};
    for (const id_chunk of chunk(id_array, 50)) {
      const ids = id_chunk.join('%2C');
      const url =
        'https://api.coingecko.com/api/v3/simple/price?ids=' +
        ids +
        '&vs_currencies=usd';

      const res: any = await this.lookUpPrices$(url).pipe(take(1)).toPromise();
      for (const [key, v] of Object.entries(res)) {
        const _v: any = v;
        if (_v?.usd) {
          prices[key] = v;
        }
      }
    }
    return prices;
  }

  async getBscPrices() {
    const idPrices = await this.lookUpPrices(bscTokens.map((x) => x.id));
    const prices = {};
    for (const bt of bscTokens)
      if (idPrices[bt.id]) prices[bt.contract] = idPrices[bt.id];
    return prices;
  }

  async loadBscChefContract() {}

  // async getBep20(App, token, address, stakingAddress) {
  //   if (address == '0x0000000000000000000000000000000000000000') {
  //     return {
  //       address,
  //       name: 'Binance',
  //       symbol: 'BNB',
  //       totalSupply: 1e8,
  //       decimals: 18,
  //       staked: 0,
  //       unstaked: 0,
  //       contract: null,
  //       tokens: [address],
  //     };
  //   }
  //   const decimals = await token.decimals();
  //   return {
  //     address,
  //     name: await token.name(),
  //     symbol: await token.symbol(),
  //     totalSupply: await token.totalSupply(),
  //     decimals: decimals,
  //     staked: (await token.balanceOf(stakingAddress)) / 10 ** decimals,
  //     unstaked: (await token.balanceOf(App.YOUR_ADDRESS)) / 10 ** decimals,
  //     contract: token,
  //     tokens: [address],
  //   };
  // }

  // async getBscToken(provider, tokenAddress, stakingAddress) {
  //   if (tokenAddress == '0x0000000000000000000000000000000000000000') {
  //     return this.getBep20(provider, null, tokenAddress, '');
  //   }
  //   const type = window.localStorage.getItem(tokenAddress);

  //   if (type) {
  //     return getBscStoredToken(provider, tokenAddress, stakingAddress, type);
  //   }

  //   try {
  //     const vpool = new ethers.Contract(
  //       tokenAddress,
  //       VALUE_LP_ABI,
  //       App.provider
  //     );
  //     const tokenWeights = await vpool.getTokenWeights();
  //     const valuePool = await getValuePool(
  //       App,
  //       vpool,
  //       tokenAddress,
  //       stakingAddress,
  //       tokenWeights
  //     );
  //     window.localStorage.setItem(tokenAddress, 'value');
  //     return valuePool;
  //   } catch (err) {}
  //   try {
  //     const pool = new ethers.Contract(tokenAddress, UNI_ABI, App.provider);
  //     const _token0 = await pool.token0();
  //     const uniPool = await getBscUniPool(
  //       App,
  //       pool,
  //       tokenAddress,
  //       stakingAddress
  //     );
  //     window.localStorage.setItem(tokenAddress, 'uniswap');
  //     return uniPool;
  //   } catch (err) {}
  //   try {
  //     const _3pool = new ethers.Contract(
  //       tokenAddress,
  //       BSC_3POOL_ABI,
  //       App.provider
  //     );
  //     const swap = await _3pool.swap();
  //     const res = await getBscSwapPool(
  //       App,
  //       _3pool,
  //       tokenAddress,
  //       stakingAddress,
  //       swap
  //     );
  //     window.localStorage.setItem(tokenAddress, 'swap');
  //     return res;
  //   } catch (err) {}
  //   try {
  //     const VAULT = new ethers.Contract(
  //       tokenAddress,
  //       BSC_VAULT_ABI,
  //       App.provider
  //     );
  //     const _token = await VAULT.token();
  //     const vault = await getBscVault(App, VAULT, tokenAddress, stakingAddress);
  //     window.localStorage.setItem(tokenAddress, 'bscVault');
  //     return vault;
  //   } catch (err) {}
  //   try {
  //     const crv = new ethers.Contract(tokenAddress, CURVE_ABI, App.provider);
  //     const minter = await crv.minter();
  //     const res = await getBscCurveToken(
  //       App,
  //       crv,
  //       tokenAddress,
  //       stakingAddress,
  //       minter
  //     );
  //     window.localStorage.setItem(tokenAddress, 'bscCurve');
  //     return res;
  //   } catch (err) {
  //     console.log(err);
  //   }
  //   try {
  //     const bep20 = new ethers.Contract(tokenAddress, ERC20_ABI, App.provider);
  //     const _name = await bep20.name();
  //     const bep20tok = await getBep20(App, bep20, tokenAddress, stakingAddress);
  //     window.localStorage.setItem(tokenAddress, 'bep20');
  //     return bep20tok;
  //   } catch (err) {
  //     console.log(`Couldn't match ${tokenAddress} to any known token type.`);
  //   }
  // }

  // async getBscStoredToken(tokenAddress, stakingAddress, type) {
  //   switch (type) {
  //     // case 'uniswap':
  //     //   const pool = provider.Contract(tokenAddress, UNI_ABI);
  //     //   return await getBscUniPool(App, pool, tokenAddress, stakingAddress);
  //     // case 'swap':
  //     //   const _3pool = new ethers.Contract(
  //     //     tokenAddress,
  //     //     BSC_3POOL_ABI,
  //     //     App.provider
  //     //   );
  //     //   const swap = await _3pool.swap();
  //     //   return await getBscSwapPool(
  //     //     App,
  //     //     _3pool,
  //     //     tokenAddress,
  //     //     stakingAddress,
  //     //     swap
  //     //   );
  //     // case 'value':
  //     //   const valuePool = new ethers.Contract(
  //     //     tokenAddress,
  //     //     VALUE_LP_ABI,
  //     //     App.provider
  //     //   );
  //     //   const tokenWeights = await valuePool.getTokenWeights();
  //     //   return await getValuePool(
  //     //     App,
  //     //     valuePool,
  //     //     tokenAddress,
  //     //     stakingAddress,
  //     //     tokenWeights
  //     //   );
  //     case 'bep20':
  //       const bep20 = provider.Contract(tokenAddress, ERC20_ABI);
  //       return await this.getBep20(App, bep20, tokenAddress, stakingAddress);
  //     // case 'bscVault':
  //     //   const vault = new ethers.Contract(
  //     //     tokenAddress,
  //     //     BSC_VAULT_ABI,
  //     //     App.provider
  //     //   );
  //     //   return await getBscVault(App, vault, tokenAddress, stakingAddress);
  //     // case 'bscCurve':
  //     //   const crv = new ethers.Contract(tokenAddress, CURVE_ABI, App.provider);
  //     //   const minter = await crv.minter();
  //     //   return await getBscCurveToken(
  //     //     App,
  //     //     crv,
  //     //     tokenAddress,
  //     //     stakingAddress,
  //     //     minter
  //     //   );
  //   }
  // }
}
