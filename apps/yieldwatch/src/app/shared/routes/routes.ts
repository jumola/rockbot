import { Routes } from '@angular/router';

export const content: Routes = [
  {
    path: 'test',
    loadChildren: () =>
      import('../../pages/test/test.module').then((m) => m.TestModule),
  },
  {
    path: '',
    loadChildren: () =>
      import('../../pages/home/home.module').then((m) => m.HomeModule),
  },
];
