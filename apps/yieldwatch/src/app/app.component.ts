import { Component, Inject, OnInit } from '@angular/core';
import { WEB3 } from './shared/web3';
import Web3 from 'web3';
import { BatchRequest, provider, Providers, Extension } from 'web3-core';
declare let window: any;
import { AbiItem } from 'web3-utils';
import { HttpClient } from '@angular/common/http';
import { chunk } from 'lodash';
import { take } from 'rxjs/operators';
import { ContractService } from './shared/service/contract/contract.service';
import {
  PANCAKE_CHEF_ABI,
  PANCAKE_CHEF_ADDR,
} from './shared/service/contract/_tmp/vars';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'yieldwatch';

  constructor(
    @Inject(WEB3) private web3: Web3,
    private contractService: ContractService
  ) {}

  async ngOnInit() {
    // @ts-ignore
    if ('enable' in this.web3.currentProvider) {
      // @ts-ignore
      await this.web3.currentProvider.enable();
      this.contractService.web3$.next(this.web3);
      await this.start();
    }
  }

  async start() {
    // test, get the account / wallet
    const account = await this.contractService.getAccount();

    console.log('account retrieved:', account, 'ok');

    this.contractService.userWalletAddress$.next(account);

    // now we want to get the LP's this asshole has in exchanges
    // at first, we'll stick with PancakeSwap until we figure how the whole
    // thing works. THEN, we'll refactor

    // so, first, we'll get CAKE's contract
    const contract = await this.contractService.getContract(
      PANCAKE_CHEF_ABI,
      PANCAKE_CHEF_ADDR
    );

    console.log('contract', contract);

    const rewardsPerWeek = await this.contractService.getRewardsPerWeek(
      contract,
      'cakePerBlock'
    );

    console.log('rewardsPerWeek', rewardsPerWeek);

    const prices = await this.contractService.getBscPrices();
    console.log('prices', prices);
  }

  //   const getBscPrices = async () => {
  //     const idPrices = await this.lookUpPrices(bscTokens.map((x) => x.id));
  //     const prices = {};
  //     for (const bt of bscTokens)
  //       if (idPrices[bt.id]) prices[bt.contract] = idPrices[bt.id];
  //     return prices;
  //   };
  //   const prices = await getBscPrices();

  //   console.log('prices', prices);

  //   console.log('tokens', tokens);

  //   const result = await this.loadBscChefContract(
  //     prices,
  //     PANCAKE_CHEF,
  //     PANCAKE_CHEF_ADDR,
  //     PANCAKE_CHEF_ABI,
  //     rewardTokenTicker,
  //     'cake',
  //     null,
  //     rewardsPerWeek,
  //     'pendingCake',
  //     null
  //   );

  //   console.log('result', result);
  // }

  // async lookUpPrices(id_array) {
  //   const prices = {};
  //   for (const id_chunk of chunk(id_array, 50)) {
  //     const ids = id_chunk.join('%2C');

  //     const url =
  //       'https://api.coingecko.com/api/v3/simple/price?ids=' +
  //       ids +
  //       '&vs_currencies=usd';

  //     const res: any = await this.http
  //       .get<any>(`${url}`)
  //       .pipe(take(1))
  //       .toPromise();
  //     for (const [key, v] of Object.entries(res)) {
  //       const _v: any = v;
  //       if (_v?.usd) {
  //         prices[key] = v;
  //       }
  //     }
  //   }
  //   return prices;
  // }

  // async loadBscChefContract(
  //   prices,
  //   chef,
  //   chefAddress,
  //   chefAbi,
  //   rewardTokenTicker,
  //   rewardTokenFunction,
  //   rewardsPerBlockFunction,
  //   rewardsPerWeekFixed,
  //   pendingRewardsFunction,
  //   deathPoolIndices
  // ) {
  //   const chefContract =
  //     chef ?? new this.web3.eth.Contract(chefAddress, chefAbi);

  //   console.log('chefContract', chefContract);

  //   const poolCount = parseInt(
  //     await chefContract.methods.poolLength().call(),
  //     10
  //   );
  //   const totalAllocPoints = await chefContract.methods
  //     .totalAllocPoint()
  //     .call();

  //   console.log(
  //     `<a href='https://bscscan.com/address/${chefAddress}' target='_blank'>Staking Contract</a>`
  //   );
  //   console.log(`Found ${poolCount} pools.\n`);

  //   console.log(`Showing incentivize-d pools only.\n`);

  //   const tokens: any = {};

  //   console.log('rewardTokenFunction', rewardTokenFunction);

  //   const rewardTokenAddress = await chefContract.methods[
  //     rewardTokenFunction
  //   ]().call();

  //   console.log('rewardTokenAddress', rewardTokenAddress);

  //   const rewardToken = await getBscToken(App, rewardTokenAddress, chefAddress);
  //   // const rewardsPerWeek =
  //   //   rewardsPerWeekFixed ??
  //   //   (((await chefContract.callStatic[rewardsPerBlockFunction]()) /
  //   //     10 ** rewardToken.decimals) *
  //   //     604800) /
  //   //     3;

  //   // const poolInfos = await Promise.all(
  //   //   [...Array(poolCount).keys()].map(
  //   //     async (x) =>
  //   //       await getBscPoolInfo(
  //   //         App,
  //   //         chefContract,
  //   //         chefAddress,
  //   //         x,
  //   //         pendingRewardsFunction
  //   //       )
  //   //   )
  //   // );

  //   // var tokenAddresses = [].concat.apply(
  //   //   [],
  //   //   poolInfos.filter((x) => x.poolToken).map((x) => x.poolToken.tokens)
  //   // );

  //   // await Promise.all(
  //   //   tokenAddresses.map(async (address) => {
  //   //     tokens[address] = await getBscToken(App, address, chefAddress);
  //   //   })
  //   // );

  //   // if (deathPoolIndices) {
  //   //   //load prices for the deathpool assets
  //   //   deathPoolIndices
  //   //     .map((i) => poolInfos[i])
  //   //     .map((poolInfo) =>
  //   //       poolInfo.poolToken
  //   //         ? getPoolPrices(tokens, prices, poolInfo.poolToken, 'bsc')
  //   //         : undefined
  //   //     );
  //   // }

  //   // const poolPrices = poolInfos.map((poolInfo) =>
  //   //   poolInfo.poolToken
  //   //     ? getPoolPrices(tokens, prices, poolInfo.poolToken, 'bsc')
  //   //     : undefined
  //   // );

  //   // console.log('Finished reading smart contracts.\n');

  //   // let aprs = [];
  //   // for (i = 0; i < poolCount; i++) {
  //   //   if (poolPrices[i]) {
  //   //     const apr = printChefPool(
  //   //       App,
  //   //       chefAbi,
  //   //       chefAddress,
  //   //       prices,
  //   //       tokens,
  //   //       poolInfos[i],
  //   //       i,
  //   //       poolPrices[i],
  //   //       totalAllocPoints,
  //   //       rewardsPerWeek,
  //   //       rewardTokenTicker,
  //   //       rewardTokenAddress,
  //   //       pendingRewardsFunction,
  //   //       null,
  //   //       null,
  //   //       'bsc'
  //   //     );
  //   //     aprs.push(apr);
  //   //   }
  //   // }
  //   // let totalUserStaked = 0,
  //   //   totalStaked = 0,
  //   //   averageApr = 0;
  //   // for (const a of aprs) {
  //   //   if (!isNaN(a.totalStakedUsd)) {
  //   //     totalStaked += a.totalStakedUsd;
  //   //   }
  //   //   if (a.userStakedUsd > 0) {
  //   //     totalUserStaked += a.userStakedUsd;
  //   //     averageApr += (a.userStakedUsd * a.yearlyAPR) / 100;
  //   //   }
  //   // }
  //   // averageApr = averageApr / totalUserStaked;
  //   // console.log(`Total Staked: $${formatMoney(totalStaked)}`);
  //   // if (totalUserStaked > 0) {
  //   //   console.log(
  //   //     `\nYou are staking a total of $${formatMoney(
  //   //       totalUserStaked
  //   //     )} at an average APR of ${(averageApr * 100).toFixed(2)}%`
  //   //   );
  //   //   console.log(
  //   //     `Estimated earnings:` +
  //   //       ` Day $${formatMoney((totalUserStaked * averageApr) / 365)}` +
  //   //       ` Week $${formatMoney((totalUserStaked * averageApr) / 52)}` +
  //   //       ` Year $${formatMoney(totalUserStaked * averageApr)}\n`
  //   //   );
  //   // }
  //   // return { prices, totalUserStaked, totalStaked, averageApr };
  // }
}
