import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TokenCreateViewEditComponent } from './token-create-view-edit.component';

describe('TokenCreateViewEditComponent', () => {
  let component: TokenCreateViewEditComponent;
  let fixture: ComponentFixture<TokenCreateViewEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TokenCreateViewEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TokenCreateViewEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
