import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import {
  distinctUntilChanged,
  map,
  switchMap,
  take,
  tap,
} from 'rxjs/operators';
import { Channel } from '@rockbot/data/typeorm';
import { ChannelService } from '../../../shared/services/channel/channel.service';
import { CoreService } from '../../../shared/services/core.service';
import { ModalService } from '../../../shared/services/modal/modal.service';
import { TokenService } from '../../../shared/services/token/token.service';

@Component({
  selector: 'app-token-create-view-edit',
  templateUrl: './token-create-view-edit.component.html',
  styleUrls: ['./token-create-view-edit.component.scss'],
})
export class TokenCreateViewEditComponent {
  constructor(
    private route: ActivatedRoute,
    private service: TokenService,
    private channelService: ChannelService,
    private modalService: ModalService,
    private router: Router,
    private coreService: CoreService
  ) {}

  public validate = false;

  mode = this.route.snapshot.data.mode;

  form = new FormGroup({
    name: new FormControl(null),
    ticker: new FormControl(null),
    address: new FormControl(null),
    channelId: new FormControl(null),
    isActive: new FormControl(null),
  });

  id$ = this.route.paramMap.pipe(map((paramMap) => paramMap.get('id')));

  item$ = this.id$.pipe(
    distinctUntilChanged(),
    switchMap((id) => this.service.get(id)),
    tap((data) =>
      this.form.patchValue({
        ...data,
      })
    )
  );

  channels$: Observable<Channel[]> = this.channelService.getList().pipe(
    take(1),
    map((value) => value ?? [])
  );

  isEditMode(): Boolean {
    return this.mode === 'edit';
  }

  async onDelete() {
    const id = await this.id$.pipe(take(1)).toPromise();
    return await this.service
      .delete(id)
      .pipe(take(1))
      .toPromise()
      .then(async () => await this.router.navigate(['channel']))
      .catch(
        async (error) => await this.coreService.showHttpErrorAsModal(error)
      );
  }

  async onSubmit() {
    const id = await this.id$.pipe(take(1)).toPromise();
    const body = this.form.value;
    const requestBase = this.isEditMode()
      ? this.service.update(id, body)
      : this.service.create(body);

    await requestBase
      .pipe(take(1))
      .toPromise()
      .then(async () => {
        const msg = this.isEditMode() ? 'Document saved' : 'Document created';
        await this.modalService.customSuccess(msg);
        if (!this.isEditMode()) {
          this.form.reset();
        }
      })
      .catch(
        async (error) => await this.coreService.showHttpErrorAsModal(error)
      );
  }
}
