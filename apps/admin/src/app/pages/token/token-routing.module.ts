import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TokenListComponent } from './token-list/token-list.component';
import { TokenCreateViewEditComponent } from './token-create-view-edit/token-create-view-edit.component';

import { TokenComponent } from './token.component';

const routes: Routes = [
  {
    path: '',
    component: TokenComponent,
    children: [
      {
        path: '',
        component: TokenListComponent,
      },
      {
        path: 'create',
        component: TokenCreateViewEditComponent,
        data: {
          mode: 'create',
        },
      },
      {
        path: 'view-edit/:id',
        component: TokenCreateViewEditComponent,
        data: {
          mode: 'edit',
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TokenRoutingModule {}
