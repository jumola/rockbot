import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';

import { TokenRoutingModule } from './token-routing.module';
import { TokenComponent } from './token.component';
import { TokenListComponent } from './token-list/token-list.component';
import { TokenCreateViewEditComponent } from './token-create-view-edit/token-create-view-edit.component';

@NgModule({
  imports: [CommonModule, SharedModule, TokenRoutingModule],
  declarations: [
    TokenComponent,
    TokenListComponent,
    TokenCreateViewEditComponent,
  ],
})
export class TokenModule {}
