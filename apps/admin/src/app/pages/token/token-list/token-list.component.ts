import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import type { Token } from '@rockbot/data/typeorm';
import { TokenService } from '../../../shared/services/token/token.service';

@Component({
  selector: 'app-token-list',
  templateUrl: './token-list.component.html',
  styleUrls: ['./token-list.component.scss'],
})
export class TokenListComponent {
  constructor(private tokenService: TokenService) {}

  items$: Observable<Token[]> = this.tokenService.getList().pipe(take(1));
}
