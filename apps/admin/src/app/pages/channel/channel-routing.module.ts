import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChannelListComponent } from './channel-list/channel-list.component';
import { ChannelCreateViewEditComponent } from './channel-create-view-edit/channel-create-view-edit.component';

import { ChannelComponent } from './channel.component';

const routes: Routes = [
  {
    path: '',
    component: ChannelComponent,
    children: [
      {
        path: '',
        component: ChannelListComponent,
      },
      {
        path: 'create',
        component: ChannelCreateViewEditComponent,
        data: {
          mode: 'create',
        },
      },
      {
        path: 'view-edit/:id',
        component: ChannelCreateViewEditComponent,
        data: {
          mode: 'edit',
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChannelRoutingModule {}
