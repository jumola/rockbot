import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  distinctUntilChanged,
  map,
  switchMap,
  take,
  tap,
} from 'rxjs/operators';
import { ChannelService } from '../../../shared/services/channel/channel.service';
import { ModalService } from '../../../shared/services/modal/modal.service';
import { CoreService } from '../../../shared/services/core.service';

@Component({
  selector: 'app-channel-create-view-edit',
  templateUrl: './channel-create-view-edit.component.html',
  styleUrls: ['./channel-create-view-edit.component.scss'],
})
export class ChannelCreateViewEditComponent {
  constructor(
    private route: ActivatedRoute,
    private channelService: ChannelService,
    private modalService: ModalService,
    private router: Router,
    private coreService: CoreService
  ) {}

  public validate = false;

  mode = this.route.snapshot.data.mode;

  form = new FormGroup({
    telegramChannelId: new FormControl(null),
  });

  id$ = this.route.paramMap.pipe(map((paramMap) => paramMap.get('id')));

  item$ = this.id$.pipe(
    distinctUntilChanged(),
    switchMap((id) => this.channelService.get(id)),
    tap((data) =>
      this.form.patchValue({
        ...data,
      })
    )
  );

  isEditMode(): Boolean {
    return this.mode === 'edit';
  }

  async onDelete() {
    const id = await this.id$.pipe(take(1)).toPromise();
    return await this.channelService
      .delete(id)
      .pipe(take(1))
      .toPromise()
      .then(async () => await this.router.navigate(['channel']))
      .catch(
        async (error) => await this.coreService.showHttpErrorAsModal(error)
      );
  }

  async onSubmit() {
    const id = await this.id$.pipe(take(1)).toPromise();
    const body = this.form.value;
    const requestBase = this.isEditMode()
      ? this.channelService.update(id, body)
      : this.channelService.create(body);

    await requestBase
      .pipe(take(1))
      .toPromise()
      .then(async () => {
        const msg = this.isEditMode() ? 'Document saved' : 'Document created';
        await this.modalService.customSuccess(msg);
        if (!this.isEditMode()) {
          this.form.reset();
        }
      })
      .catch(
        async (error) => await this.coreService.showHttpErrorAsModal(error)
      );
  }
}
