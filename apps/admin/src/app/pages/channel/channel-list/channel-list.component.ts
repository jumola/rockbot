import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import type { Channel } from '@rockbot/data/typeorm';
import { ActivatedRoute } from '@angular/router';
import { ChannelService } from '../../../shared/services/channel/channel.service';

@Component({
  selector: 'app-channel-list',
  templateUrl: './channel-list.component.html',
  styleUrls: ['./channel-list.component.scss'],
})
export class ChannelListComponent implements OnInit {
  constructor(
    private channelService: ChannelService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {}

  items$: Observable<Channel[]> = this.channelService.getList().pipe(take(1));
}
