import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';

import { ChannelRoutingModule } from './channel-routing.module';
import { ChannelComponent } from './channel.component';
import { ChannelListComponent } from './channel-list/channel-list.component';
import { ChannelCreateViewEditComponent } from './channel-create-view-edit/channel-create-view-edit.component';

@NgModule({
  imports: [CommonModule, SharedModule, ChannelRoutingModule],
  declarations: [
    ChannelComponent,
    ChannelListComponent,
    ChannelCreateViewEditComponent,
  ],
})
export class ChannelModule {}
