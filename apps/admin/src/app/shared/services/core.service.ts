import { Injectable } from '@angular/core';
import { ModalService } from './modal/modal.service';

@Injectable({
  providedIn: 'root',
})
export class CoreService {
  constructor(private modalService: ModalService) {}

  async showHttpErrorAsModal(error) {
    await this.modalService.customError(
      (error?.error?.message ?? []).join('\n')
    );
  }
}
