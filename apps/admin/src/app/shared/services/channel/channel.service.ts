import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@rockbot/environment';
import { Channel } from '@rockbot/data/typeorm';

@Injectable({
  providedIn: 'root',
})
export class ChannelService {
  private apiBaseURL = `${environment.API_BASE_URL}`;
  private apiEndpoint = 'channel';

  constructor(private http: HttpClient) {}

  get = (id) =>
    this.http
      .get<Channel>(`${this.apiBaseURL}/${this.apiEndpoint}/${id}`)
      .pipe();

  getList = () =>
    this.http.get<any>(`${this.apiBaseURL}/${this.apiEndpoint}`).pipe();

  update = (id, body) =>
    this.http
      .patch<any>(`${this.apiBaseURL}/${this.apiEndpoint}/${id}`, body)
      .pipe();

  create = (body) =>
    this.http.post<any>(`${this.apiBaseURL}/${this.apiEndpoint}`, body).pipe();

  delete = (id) =>
    this.http
      .delete<any>(`${this.apiBaseURL}/${this.apiEndpoint}/${id}`)
      .pipe();
}
