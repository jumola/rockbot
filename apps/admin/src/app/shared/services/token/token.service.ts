import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Token } from '@rockbot/data/typeorm';
import { environment } from '@rockbot/environment';

@Injectable({
  providedIn: 'root',
})
export class TokenService {
  private apiBaseURL = `${environment.API_BASE_URL}`;
  private apiEndpoint = 'token';

  constructor(private http: HttpClient) {}

  get = (id) =>
    this.http.get<Token>(`${this.apiBaseURL}/${this.apiEndpoint}/${id}`).pipe();

  getList = () =>
    this.http.get<Token[]>(`${this.apiBaseURL}/${this.apiEndpoint}`).pipe();

  update = (id, body) =>
    this.http
      .patch<any>(`${this.apiBaseURL}/${this.apiEndpoint}/${id}`, body)
      .pipe();

  create = (body) =>
    this.http.post<any>(`${this.apiBaseURL}/${this.apiEndpoint}`, body).pipe();

  delete = (id) =>
    this.http
      .delete<any>(`${this.apiBaseURL}/${this.apiEndpoint}/${id}`)
      .pipe();
}
