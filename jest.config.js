module.exports = {
  projects: [
    '<rootDir>/apps/rockbot',
    '<rootDir>/apps/admin',
    '<rootDir>/apps/api',
    '<rootDir>/libs/data/typeorm',
    '<rootDir>/libs/data/typeorm-shared',
    '<rootDir>/libs/data/constants',
    '<rootDir>/libs/environment',
    '<rootDir>/libs/shared/types',
    '<rootDir>/libs/features/bsc/api',
    '<rootDir>/apps/yieldwatch',
  ],
};
